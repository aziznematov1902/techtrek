// src/App.js
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ProductPage from './components/ProductPage';
import OrderForm from './components/OrderForm';
import NewsFeed from './components/NewsFeed';
import ContactInfo from './components/ContactInfo';
import Navbar from './components/Navbar';
import './App.css';

function App() {
    return (
        <Router>
            <div className="App">
                <Navbar />
                <Routes>
                    <Route path="/" element={<ProductPage />} />
                    <Route path="/pre-order" element={<OrderForm />} />
                    <Route path="/news" element={<NewsFeed />} />
                    <Route path="/contact" element={<ContactInfo />} />
                </Routes>
            </div>
        </Router>
    );
}

export default App;
