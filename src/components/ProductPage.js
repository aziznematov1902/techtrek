import React from 'react';
import Slideshow from './Slideshow'; // Import the Slideshow component
import { Link } from 'react-router-dom'; // Import Link for navigation
import './ProductPage.css';

const ProductPage = () => {
    const images = [
        'https://images.unsplash.com/photo-1472214103451-9374bd1c798e?q=80&w=1470&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
        'https://images.unsplash.com/photo-1555543451-eeaff357e0f3?q=80&w=1631&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
        'https://images.unsplash.com/photo-1605379399642-870262d3d051?q=80&w=1506&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    ];

    const singleNewsItem = {
        title: "TechTrek X Release Date Announced!",
        date: "June 1, 2024",
        content: "The long-awaited TechTrek X will be available starting from July 1, 2024. Pre-orders are now open!",
        image: "https://images.unsplash.com/photo-1451187580459-43490279c0fa?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8dGVjaG5vbG9neXxlbnwwfHwwfHx8MA%3D%3Dg",
    };

    return (
        <div className="product-page">
            <div className="slideshow-container">
                <Slideshow images={images} />
                <div className="slide-content">
                    <img
                        src="https://lirp.cdn-website.com/40446ade/dms3rep/multi/opt/Tech-Trek+Ltd_Logo_White+copy-277w.png"
                        alt="TechTrek X"
                        className="centered-image"
                    />
                    <h1>TECH-TREK LTD</h1>
                    <p>Canadian Electronic Component Manufacturer Representative</p>
                </div>
            </div>
            <h1>News</h1>
            <div className="product-content">
                <div className="news-section">
                    <div className="single-news-item">
                        <img src={singleNewsItem.image} alt={singleNewsItem.title} />
                        <h2>{singleNewsItem.title}</h2>
                        <p><em>{singleNewsItem.date}</em></p>
                        <p>{singleNewsItem.content}</p>
                        <Link to="/news" className="more-button">More</Link>
                    </div>
                </div>
                <div className="info-section">
                    <img src="https://images.unsplash.com/photo-1523371683773-affcb4a2e39e?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fHNtYXJ0cGhvbmVzfGVufDB8fDB8fHww"/>
                    <h2>About TechTrek X</h2>
                    <p>The TechTrek X is our latest smartphone offering, featuring cutting-edge technology and innovative features. It’s designed to provide the best user experience, with a sleek design, powerful performance, and exceptional camera quality.</p>
                    <Link to="/Pre-order" className="order-button">Order Now</Link>
                </div>
            </div>
            <div className="contact-section">
                <h2>Contact Us</h2>
                <p>Email: contact@techtrek.com</p>
                <p>Phone: +1 234 567 890</p>
                <Link to="/contact" className="contact-button">Contact Us</Link>
            </div>
            <footer className="footer">
                <p>&copy; 2024 TechTrek. All rights reserved.</p>
            </footer>
        </div>
    );
};

export default ProductPage;
