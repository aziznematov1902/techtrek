import React from 'react';
import './NewsFeed.css'; // Import CSS for styling

const NewsFeed = () => {
    // Mock data for news items
    const newsItems = [
        {
            id: 1,
            title: "TechTrek X Release Date Announced",
            description: "TechTrek, the innovative startup known for its cutting-edge tracking and management systems, has officially announced the release date for its highly anticipated smartphone, the TechTrek X. The device is set to hit the market on July 15th, promising revolutionary features and advanced capabilities to redefine the smartphone experience.",
            imageUrl: "https://images.unsplash.com/photo-1451187580459-43490279c0fa?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8dGVjaG5vbG9neXxlbnwwfHwwfHx8MA%3D%3Dg" // URL for news item image
        },
        {
            id: 2,
            title: "TechTrek X Receives Rave Reviews",
            description: "Following its unveiling, the TechTrek X smartphone has garnered widespread praise from tech experts and industry insiders. With its sleek design, powerful performance, and innovative features, the TechTrek X is being hailed as a game-changer in the world of mobile technology, setting new standards for excellence and innovation.",
            imageUrl: "https://images.unsplash.com/photo-1528795259021-d8c86e14354c?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTl8fG1vYmlsZXxlbnwwfHwwfHx8MA%3D%3D" // URL for news item image
        },
        {
            id: 3,
            title: "TechTrek Partners with Leading Telecom Providers",
            description: "In a strategic move to enhance the launch of the TechTrek X smartphone, TechTrek has forged partnerships with leading telecom providers to offer exclusive deals and promotions to customers. With attractive bundles, discounts, and incentives, customers can enjoy unparalleled value and savings when they pre-order the TechTrek X through select carriers.",
            imageUrl: "https://images.unsplash.com/photo-1485827404703-89b55fcc595e?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTl8fHRlY2hub2xvZ3l8ZW58MHx8MHx8fDA%3D" // URL for news item image
        }
    ];

    return (
        <div className="news-feed">
            {newsItems.map(newsItem => (
                <div key={newsItem.id} className="news-item">
                    <img src={newsItem.imageUrl} alt={newsItem.title} className="news-item-image" />
                    <div className="news-item-details">
                        <h3>{newsItem.title}</h3>
                        <p>{newsItem.description}</p>
                    </div>
                </div>
            ))}
        </div>
    );
};

export default NewsFeed;
