// src/components/Navbar.js
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';

const Navbar = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggleMenu = () => {
        setIsOpen(!isOpen);
    };

    return (
        <nav className="navbar">
            <div className="navbar-brand">
                <img
                    src="https://media.licdn.com/dms/image/C510BAQEEr9aADQWsgA/company-logo_200_200/0/1631310464656?e=2147483647&v=beta&t=L-TqXLLi6IuDopf27QTSewUr_zXljzbn_6dLqOFBeJY"
                    style={{width: '80px', height: '45px'}} alt="TechTrek X"/>
                <div className="navbar-title-text">
                <Link to="/">TechTrek X</Link></div>
            </div>
            <div className="burger-menu" onClick={toggleMenu}>
                <div className={isOpen ? "bar open" : "bar"}></div>
                <div className={isOpen ? "bar open" : "bar"}></div>
                <div className={isOpen ? "bar open" : "bar"}></div>
            </div>
            <ul className={isOpen ? "navbar-links open" : "navbar-links"}>
                <li><Link to="/" onClick={toggleMenu}>Home</Link></li>
                <li><Link to="/pre-order" onClick={toggleMenu}>Pre-Order</Link></li>
                <li><Link to="/news" onClick={toggleMenu}>News</Link></li>
                <li><Link to="/contact" onClick={toggleMenu}>Contact</Link></li>
            </ul>
        </nav>
    );
};

export default Navbar;
