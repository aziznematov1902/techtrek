import React from 'react';
import './ContactInfo.css';

const Contact = () => {
    return (
        <div className="contact-container">
            <div className="contact-info">
                <h2>Contact Information</h2>
                <p><strong>Address:</strong> 123 TechTrek Lane, Innovation City, Techland</p>
                <p><strong>Phone:</strong> (123) 456-7890</p>
                <p><strong>Email:</strong> contact@techtrek.com</p>
            </div>
            <div className="contact-form-wrapper">
                <h2>Contact Us</h2>
                <form className="contact-form">
                    <label htmlFor="name">Name</label>
                    <input type="text" id="name" name="name" required />

                    <label htmlFor="email">Email</label>
                    <input type="email" id="email" name="email" required />

                    <label htmlFor="message">Message</label>
                    <textarea id="message" name="message" rows="4" required></textarea>

                    <button type="submit">Submit</button>
                </form>
            </div>
        </div>
    );
};

export default Contact;
